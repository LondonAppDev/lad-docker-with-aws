# LAD Docker with AWS

A docker image based on `docker:19` which contains the following:

 - `Python 3` installed and set as default Python version
 - `awscli==1.17.11` installed

This is intended to be used for CI tools.

## Usage

To pull image:

```
docker pull londonappdev/docker-with-aws:latest
```

*Go to [tags](https://hub.docker.com/r/londonappdev/docker-with-aws/tags) to see available versions.*

To run image:

```
docker run londonappdev/docker-with-aws:latest aws --version
```

## Contributions

Contributions are welcome via the public [GitLab repository](https://gitlab.com/LondonAppDev/lad-docker-with-aws).

This project uses [GitLab Flow](https://about.gitlab.com/solutions/gitlab-flow/) for deployment.
