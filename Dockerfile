FROM docker:19
LABEL maintainer="mark@londonappdeveloper.com"

RUN apk add --update --no-cache python3
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN pip3 install awscli==1.17.11
